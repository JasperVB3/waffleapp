package com.example.waffleapp

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_completed_order.*

class CompletedOrderActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_completed_order)

        back_to_the_start.setOnClickListener {
            startActivity(Intent( this, ChoiseMenuActivity::class.java))
            finish()
        }
    }
}
