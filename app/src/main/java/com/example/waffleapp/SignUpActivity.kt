package com.example.waffleapp

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Patterns
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.sign_up
import kotlinx.android.synthetic.main.activity_sign_up.*
import java.util.regex.PatternSyntaxException


class SignUpActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        auth = FirebaseAuth.getInstance()

        sign_up.setOnClickListener{
            signUpUser()
        }
    }
       private fun signUpUser(){
            if (tv_email.text.toString().isEmpty()) {
                tv_email.error = "Please enter email"
                tv_email.requestFocus()
                return
            }
            if(!Patterns.EMAIL_ADDRESS.matcher(tv_email.text.toString()).matches()){
                tv_email.error ="Please enter a valid email"
                tv_email.requestFocus()
                return
            }
            if(tv_password.text.toString().isEmpty()){
                tv_password.error = "Please enter email"
                tv_password.requestFocus()
                return
            }
           auth.createUserWithEmailAndPassword(tv_email.text.toString(), tv_password.text.toString())
               .addOnCompleteListener(this) { task ->
                   if (task.isSuccessful) {
                       val user = auth.currentUser
                       user?.sendEmailVerification()
                           ?.addOnCompleteListener { task ->
                               if (task.isSuccessful) {
                                   startActivity(Intent(this,MainActivity::class.java))
                                   finish()
                               }
                           }

                   } else {
                       Toast.makeText(baseContext, "Sign Up failed. Try again after some time.",
                           Toast.LENGTH_SHORT).show()
                   }

                   // ...
               }
        }

        }

