package com.example.waffleapp

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_delete_account.*
import kotlinx.android.synthetic.main.activity_list_of_waffels.*

class DeleteAccountActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delete_account)

        cancel_deleting_password.setOnClickListener {
            startActivity(Intent(this, ChoiseMenuActivity::class.java))
            finish()
        }

        yes.setOnClickListener {
            deleteAccount()
        }
}
    private fun deleteAccount(){
        val user: FirebaseUser? = FirebaseAuth.getInstance().currentUser
        user?.delete()
            ?.addOnCompleteListener {
                if (it.isSuccessful) {
                    startActivity(Intent(this, MainActivity::class.java))
                    Toast.makeText(this, "Account succesfull deleted.",Toast.LENGTH_SHORT)
                        .show()
                    finish()



                }else{
                    Toast.makeText(this, "Something went wrong... Try again",Toast.LENGTH_SHORT)
                        .show()
                }
            }
    }
}


