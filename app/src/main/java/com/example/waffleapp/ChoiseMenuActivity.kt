package com.example.waffleapp

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_choise_menu.*
import kotlinx.android.synthetic.main.activity_main.*

class ChoiseMenuActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choise_menu)

        our_products.setOnClickListener {
            startActivity(Intent(this,ListOfWaffels::class.java))
            finish()
        }

        shopping_cart.setOnClickListener {
            startActivity(Intent(this,ShoppingCartActivity::class.java))
            finish()
        }
        account.setOnClickListener{
            startActivity(Intent(this, UserActivity::class.java))
            finish()
        }
        logout.setOnClickListener{
            FirebaseAuth.getInstance().signOut()
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }
}
