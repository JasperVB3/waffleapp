package com.example.waffleapp

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_choise_menu.*
import kotlinx.android.synthetic.main.activity_list_of_waffels.*
import kotlinx.android.synthetic.main.activity_user.*

class UserActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)

        button_back_home.setOnClickListener{
            startActivity(Intent(this,ChoiseMenuActivity::class.java))
            finish()
        }
        change_password.setOnClickListener {
            startActivity(Intent(this, ChangePasswordActivity::class.java))
            finish()
        }
        delete_account.setOnClickListener {
            startActivity(Intent(this, DeleteAccountActivity::class.java))
            finish()
        }
        logout_second.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }

}
