package com.example.waffleapp

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import data.SortsListAdapter
import kotlinx.android.synthetic.main.activity_list_of_waffels.*
import model.SortModel

class ListOfWaffels : AppCompatActivity() {
    private var adapter: SortsListAdapter?=null
    private var sortList: ArrayList<SortModel>?=null
    private var layoutManager: RecyclerView.LayoutManager?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_of_waffels)

        cancel.setOnClickListener{
            startActivity(Intent(this,ChoiseMenuActivity::class.java))
            finish()
        }
        buy.setOnClickListener {
            startActivity(Intent(this, ShoppingCartActivity::class.java))
            finish()
        }

        sortList = ArrayList<SortModel>()
        layoutManager = LinearLayoutManager(this) as RecyclerView.LayoutManager?
        adapter = SortsListAdapter(sortList!!, this)


        rcv.layoutManager= layoutManager
        rcv.adapter = adapter
        var sortNameList:Array<String> = arrayOf("CHOCO WAFFLE", "VANILLE WAFFLE", "FRANCHIPAN", "SQUAREJAM", "MIX")
        var sortPriceList:Array<String> = arrayOf("€4/BOX", "€3/BOX","€5/BOX", "€5/BOX", "€6/BOX")


        for(i in 0..4){
            var waffle = SortModel()
            waffle.waffleName = sortNameList[i]
            waffle.wafflePrice =sortPriceList[i]
            sortList?.add(waffle)


        }
        adapter!!.notifyDataSetChanged()

    }


 }