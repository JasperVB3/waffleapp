package com.example.waffleapp

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_shopping_cart.*

class ShoppingCartActivity : AppCompatActivity() {

    lateinit var firstname: EditText
    lateinit var lastname: EditText
    lateinit var street_name: EditText
    lateinit var house_number: EditText
    lateinit var town: EditText
    lateinit var postal_code: EditText
    lateinit var country: EditText
    lateinit var confirm_order: Button
    lateinit var choco_waffles_order: EditText
    lateinit var vanille_waffles_order: EditText
    lateinit var frangipan_order: EditText
    lateinit var squarejam_order: EditText
    lateinit var mix_order: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shopping_cart)

        firstname = findViewById(R.id.firstname)
        lastname = findViewById(R.id.lastname)
        street_name = findViewById(R.id.street_name)
        house_number = findViewById(R.id.house_number)
        town = findViewById(R.id.town)
        postal_code = findViewById(R.id.postal_code)
        country = findViewById(R.id.country)
        confirm_order = findViewById(R.id.confirm_order)
        choco_waffles_order = findViewById(R.id.choco_waffles_order)
        vanille_waffles_order = findViewById(R.id.vanille_waffles_order)
        frangipan_order = findViewById(R.id.frangipan_order)
        squarejam_order = findViewById(R.id.squarejam_order)
        mix_order = findViewById(R.id.mix_order)


        confirm_order.setOnClickListener {
            saveOrder()
        }
        cancel_order.setOnClickListener {
            startActivity(Intent(this, ChoiseMenuActivity::class.java))
            finish()
        }

    }
    private fun saveOrder() {
        val name = firstname.text.toString().trim()
        val secondName = lastname.text.toString().trim()
        val strName = street_name.text.toString().trim()
        val hsNumber = house_number.text.toString().trim()
        val twn = town.text.toString().trim()
        val postCode = postal_code.text.toString().trim()
        val cntry = country.text.toString().trim()
        val choco = choco_waffles_order.text.toString().trim()
        val vanille = vanille_waffles_order.text.toString().trim()
        val frangi = frangipan_order.text.toString().trim()
        val squarej = squarejam_order.text.toString().trim()
        val mix = mix_order.text.toString().trim()


        if(name.isEmpty()){
            firstname.error = "Please enter your first name."
            return
        }
        if(secondName.isEmpty()){
            firstname.error = "Please enter your last name."
            return
        }
        if(strName.isEmpty()){
            firstname.error = "Please enter your street name."
            return
        }
        if(hsNumber.isEmpty()){
            firstname.error = "Please enter your house number."
            return
        }
        if(twn.isEmpty()){
            firstname.error = "Please enter your town."
            return
        }
        if(postCode.isEmpty()){
            firstname.error = "Please enter your postal code."
            return
        }
        if(cntry.isEmpty()){
            firstname.error = "Please enter your country."
            return
        }



        val ref = FirebaseDatabase.getInstance().getReference("orders")
        val orderId = ref.push().key

        val order = Order(orderId.toString(), name, secondName, strName, hsNumber, twn, postCode, cntry, choco, vanille, frangi, squarej, mix)

        ref.child(orderId.toString()).setValue(order).addOnCompleteListener {
            Toast.makeText(applicationContext, "Order saved successfully", Toast.LENGTH_LONG).show()
        }
        startActivity(Intent(this, CompletedOrderActivity::class.java))
        finish()
    }


}
