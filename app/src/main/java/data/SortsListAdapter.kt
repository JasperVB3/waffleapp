package data

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.waffleapp.R
import com.example.waffleapp.ThisWaffle
import model.SortModel

class SortsListAdapter (private val list:ArrayList<SortModel>, private val context: Context)
    : RecyclerView.Adapter<SortsListAdapter.ViewHolder>() {

    inner class ViewHolder (itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bindItem(s: SortModel) {
            var sort: TextView = itemView.findViewById(R.id.waffle) as TextView
            sort.text = s.waffleName

            var sortPrice: TextView = itemView.findViewById(R.id.price) as TextView
            sortPrice.text = s.wafflePrice

            /*itemView.setOnClickListener {
                val intent = Intent(context, ThisWaffle::class.java)
                context.startActivity(intent)

            }*/

        }




    }override fun onCreateViewHolder(parent: ViewGroup, position: Int): SortsListAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.activity_sorts, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: SortsListAdapter.ViewHolder, position: Int) {
        holder.bindItem(list[position])
    }



}